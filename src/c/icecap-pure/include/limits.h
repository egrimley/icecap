#pragma once

#define CHAR_BIT 8
#define UCHAR_MAX 255
#define INT_MAX 0x7fffffff
#define INT_MIN (-1-INT_MAX)
#define UINT_MAX 0xffffffffu
