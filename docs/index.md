# Documentation

`TODO`

In the meantime, we are eager to share and discuss any aspect of IceCap's design
and implementation with you directly. Please feel free to reach out to project
lead [Nick Spinale &lt;nick.spinale@arm.com&gt;](mailto:nick.spinale@arm.com).
